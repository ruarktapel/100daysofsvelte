import { writable } from 'svelte/store';

export const nodeTree = writable([
        
        {
			containerId: 1, 
			name: "Container", 
			background: "white", 
			height:"100px", 
			width: "800px", 
			flexflow: "row", 
			parentId: 0, 
			flex: "initial", 
			justifycontent: "flex-start", 
			alignitems: "center",
			gap: "initial",
			children: []}
]);

export const clickedContainerId = writable(0);
export const resetClick = writable(0);
export const nodeTreeCount = writable(7);